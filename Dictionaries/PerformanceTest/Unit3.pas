unit Unit3;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  System.Generics.Collections, uIntDictionary;

type
  TMyObj = class

  end;

  TObjRec = TPair<Integer, TMyObj>;

  TMyGenericDic = class(TObjectDictionary<Integer, TMyObj>)
  end;

  TForm3 = class(TForm)
    btnAddElements: TButton;
    Button2: TButton;
    Memo1: TMemo;
    btnLoadFactor: TButton;
    btnInstanceSize: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnAddElementsClick(Sender: TObject);
    procedure btnLoadFactorClick(Sender: TObject);
    procedure btnInstanceSizeClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
    FGenDic: TMyGenericDic;
    FIntDic: TIntDictionary;
    procedure Log(const s: string);
    procedure AddElements;
    procedure FindElements;
    procedure ExtractPairs;
    procedure Clear;
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

uses
  DateUtils;

const
  ListSize = 1000000;

var
  H1, H2: TDateTime;

procedure TForm3.btnAddElementsClick(Sender: TObject);
begin
  AddElements;
  FindElements;
  ExtractPairs;
  Clear;
end;

procedure TForm3.btnInstanceSizeClick(Sender: TObject);
begin
  Log('Hash Table instance size: ' + IntToStr(FIntDic.InstanceSize));
  Log('TObjectDictionary instance size: ' + IntToStr(FIntDic.InstanceSize));
end;

procedure TForm3.btnLoadFactorClick(Sender: TObject);
begin
  Log('Hash Table Load Factor: ' + FloatToStr(FIntDic.LoadFactor));
end;

procedure TForm3.Button2Click(Sender: TObject);
begin
  FIntDic.Capacity := FIntDic.Capacity * 2;
end;

procedure TForm3.FormCreate(Sender: TObject);
begin
  FGenDic := TMyGenericDic.Create([doOwnsValues], ListSize div 40);
  FIntDic := TIntDictionary.Create(True, ListSize div 40);
end;

procedure TForm3.FormDestroy(Sender: TObject);
begin
  FGenDic.Free;
  FIntDic.Free;
end;

procedure TForm3.Log(const s: string);
begin
  Memo1.Lines.Add(s);
end;

procedure TForm3.AddElements;
var
  i: Integer;
begin
  Log('AddElements --------------------');
  H1 := Now;
  for i := 1 to ListSize do
  begin
    FIntDic.Add(i, TMyObj.Create);
  end;
  H2 := Now;
  Log('TIntDictionary: ' + IntToStr(DateUtils.MilliSecondsBetween(H1, H2)));
  H1 := Now;
  for i := 1 to ListSize do
  begin
    FGenDic.Add(i, TMyObj.Create);
  end;
  H2 := Now;
  Log('TDictionary: ' + IntToStr(DateUtils.MilliSecondsBetween(H1, H2)));
  Log('');
end;

procedure TForm3.FindElements;
var
  i: Integer;
begin
  Log('FindElements --------------------');
  H1 := Now;
  for i := ListSize downto 1 do
  begin
    if not FIntDic.ContainsKey(i) then
      raise Exception.Create('TIntDictionary Key not found: ' + IntToStr(i));
  end;
  H2 := Now;
  Log('TIntDictionary: ' + IntToStr(DateUtils.MilliSecondsBetween(H1, H2)));
  H1 := Now;
  for i := ListSize downto 1 do
  begin
    if not FGenDic.ContainsKey(i) then
      raise Exception.Create('TDictionary Key not found: ' + IntToStr(i));
  end;
  H2 := Now;
  Log('TDictionary: ' + IntToStr(DateUtils.MilliSecondsBetween(H1, H2)));
  Log('');
end;

procedure TForm3.ExtractPairs;
var
  Item2: TObjRec;
  i: Integer;
  Item1: TIntKeyValueItem;
begin
  Log('ExtractPairs --------------------');
  H1 := Now;
  for i := 1 to ListSize do
  begin
    Item1 := FIntDic.ExtractPair(i);
    if Assigned(Item1.Value) then
      Item1.Value.Free;
  end;
  H2 := Now;
  Log('TIntDictionary: ' + IntToStr(DateUtils.MilliSecondsBetween(H1, H2)));
  H1 := Now;
  for i := 1 to ListSize do
  begin
    Item2 := FGenDic.ExtractPair(i);
    if Assigned(Item2.Value) then
      Item2.Value.Free;
  end;
  H2 := Now;
  Log('TDictionary: ' + IntToStr(DateUtils.MilliSecondsBetween(H1, H2)));
  Log('');
end;

procedure TForm3.Clear;
begin
  Log('Clear --------------------');
  H1 := Now;
  FIntDic.Clear;
  H2 := Now;
  Log('TIntDictionary: ' + IntToStr(DateUtils.MilliSecondsBetween(H1, H2)));
  H1 := Now;
  FGenDic.Clear;
  H2 := Now;
  Log('TDictionary: ' + IntToStr(DateUtils.MilliSecondsBetween(H1, H2)));
  Log('');
end;

end.
