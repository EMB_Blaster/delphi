unit uIntDictionary;

{******************************************************************************

TIntDictionary class, a replacement for TDictionary<Integer, TObject> or TObjectDictionary<Integer, TObject>

Filename..........: uIntDictionary.pas
Version...........: 1.0
Author............: Alexandre C. Machado
Target compilers..: Delphi 2007 to Delphi 10 Seattle.
                    Will work with previous versions (down to Delphi 5 at least) with minor modifications
                    (replace the class of the class var with a field)
Date..............: Jun/2015
Description.......: A replacement for TDictionary<Integer, TObject> or TObjectDictionary<Integer, TObject>
                    Basically it faster than TDictionary which does not use generics at all
                    This class resolves collision through a bucketing mechanism
Licensing stuff...: You may use this software in any kind of development,
                    including commercial, redistribute and modify it freely.
                    This software is provided as it is, without any kind of
                    warranty given. Use it at Your own risk.
******************************************************************************}
interface

uses
  Classes, SysUtils, Types;

type
  TIntKeyValueItem = record
    Key: Integer;
    Value: TObject;
  end;

  TIntKeyValueArray = array of TIntKeyValueItem;

  TBucketArray = array of TIntKeyValueArray;

  TIntDictionary = class
  private
    FCapacity: Integer;
    FBuckets: TBucketArray;
    FOwnsObjects: Boolean;
    FCount: Integer;
    function GetLoadFactor: Double;
    procedure Resize(NewCapacity: Integer);
    procedure SetCapacity(const Value: Integer);
  private
    class var FEmptyItem: TIntKeyValueItem;   // just an empty item for comparison
  protected
    property Buckets: TBucketArray read FBuckets;
  public
    constructor Create(AOwnsObjects: Boolean; ACapacity: Integer);
    destructor Destroy; override;
    procedure Add(const AKey: Integer; AValue: TObject);
    function ContainsKey(const AKey: Integer): Boolean;
    function ContainsValue(AValue: TObject): Boolean;
    function TryGetValue(const AKey: Integer; out AValue: TObject): Boolean;
    procedure Remove(const AKey: Integer);
    function ExtractPair(const AKey: Integer): TIntKeyValueItem;
    procedure AddOrSetValue(const AKey: Integer; const AValue: TObject);
    procedure Clear;
    property LoadFactor: Double read GetLoadFactor;
    property Capacity: Integer read FCapacity write SetCapacity;
    property OwnsObjects: Boolean read FOwnsObjects write FOwnsObjects;
    property Count: Integer read FCount;
  end;

  EIntDictionaryException = class(Exception);

implementation

const
  SizeOfItem = SizeOf(TIntKeyValueItem);

{ TIntDictionary }

constructor TIntDictionary.Create(AOwnsObjects: Boolean; ACapacity: Integer);
begin
  inherited Create;
  FOwnsObjects := AOwnsObjects;
  FCount := 0;
  Capacity := ACapacity;
end;

destructor TIntDictionary.Destroy;
begin
  Clear;
  SetLength(FBuckets, 0);
  inherited;
end;

procedure TIntDictionary.Add(const AKey: Integer; AValue: TObject);
var
  Len: Integer;
  i, j, EmptyIdx: Integer;
begin
  if FCount >= (10 * FCapacity) then
    Capacity := FCount;

  i := AKey mod FCapacity;

  if i > -1 then
  begin
    Len := Length(FBuckets[i]);

    if Len = 0 then
    begin
      SetLength(FBuckets[i], 1);  // start with 1 and double the size of the bucket every time we need it to grow
      EmptyIdx := 0;
    end
    else
    begin
      for j := Low(FBuckets[i]) to High(FBuckets[i]) do
      begin
        if AKey = TIntKeyValueItem(FBuckets[i][j]).Key then
          raise EIntDictionaryException.Create('Duplicates not allowed');
      end;

      EmptyIdx := High(FBuckets[i]);
      for j := Low(FBuckets[i]) to High(FBuckets[i]) do
      begin
        // compare the memory of our empty item with the bucket item and discover if it's empty
        if CompareMem(@FEmptyItem, @FBuckets[i][j], SizeOfItem) then
        begin
          EmptyIdx := j;
          Break;
        end;
      end;

      if EmptyIdx = High(FBuckets[i]) then
      begin
        SetLength(FBuckets[i], Len * 2);
        EmptyIdx := Len;
      end;
    end;

    FBuckets[i][EmptyIdx].Key := AKey;
    FBuckets[i][EmptyIdx].Value := AValue;
    Inc(FCount);
  end;
end;

function TIntDictionary.ContainsKey(const AKey: Integer): Boolean;
var
  i, j: Integer;
begin
  Result := False;
  i := AKey mod FCapacity;

  for j := Low(FBuckets[i]) to High(FBuckets[i]) do
  begin
    if AKey = FBuckets[i][j].Key then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

function TIntDictionary.ContainsValue(AValue: TObject): Boolean;
var
  i, j: Integer;
begin
  Result := false;
  for i := Low(FBuckets) to High(FBuckets) do
  begin
    for j := Low(FBuckets[i]) to High(FBuckets[i]) do
    begin
      if TIntKeyValueItem(FBuckets[i][j]).Value = AValue then
      begin
        Result := True;
        Exit;
      end;
    end;
  end;
end;

function TIntDictionary.TryGetValue(const AKey: Integer; out AValue: TObject): Boolean;
var
  i, j: Integer;
begin
  Result := False;
  AValue := nil;
  i := AKey mod FCapacity;

  for j := Low(FBuckets[i]) to High(FBuckets[i]) do
  begin
    if AKey = FBuckets[i][j].Key then
    begin
      Result := True;
      AValue := FBuckets[i][j].Value;
      Exit;
    end;
  end;
end;

procedure TIntDictionary.Remove(const AKey: Integer);
var
  i, j: Integer;
begin
  i := AKey mod FCapacity;

  for j := Low(FBuckets[i]) to High(FBuckets[i]) do
  begin
    if AKey = FBuckets[i][j].Key then
    begin
      FillChar(FBuckets[i][j], SizeOfItem, 0);
      Dec(FCount);
      Exit;
    end;
  end;
end;

function TIntDictionary.ExtractPair(const AKey: Integer): TIntKeyValueItem;
var
  i, j: Integer;
begin
  FillChar(Result, SizeOf(Result), 0);
  i := AKey mod FCapacity;

  for j := Low(FBuckets[i]) to High(FBuckets[i]) do
  begin
    if AKey = FBuckets[i][j].Key then
    begin
      Result.Key := AKey;
      Result.Value := FBuckets[i][j].Value;
      FillChar(FBuckets[i][j], SizeOfItem, 0);
      Dec(FCount);
      Exit;
    end;
  end;
end;

function TIntDictionary.GetLoadFactor: Double;
begin
// TODO
  Result := 0.0;
end;

procedure TIntDictionary.AddOrSetValue(const AKey: Integer; const AValue: TObject);
var
  i, j: Integer;
begin
  i := AKey mod FCapacity;

  for j := Low(FBuckets[i]) to High(FBuckets[i]) do
  begin
    if AKey = FBuckets[i][j].Key then
    begin
      FBuckets[i][j].Value := AValue;
      Exit;
    end;
  end;
  // if we got that far, the key is not here yet
  Add(AKey, AValue);
end;

procedure TIntDictionary.Clear;
var
  i, j: Integer;
begin
  for i := Low(FBuckets) to High(FBuckets) do
  begin
    if OwnsObjects then
    begin
      for j := Low(FBuckets[i]) to High(FBuckets[i]) do
      begin
        if FBuckets[i][j].Value <> nil then
          FBuckets[i][j].Value.Free;
      end;
    end;
    // zeroing memory just to be safe
    FillChar(FBuckets[i][0], Length(FBuckets[i])*SizeOfItem, 0);
    SetLength(FBuckets[i], 0);
  end;
  SetLength(FBuckets, FCapacity);  // back to original capacity
  FCount := 0;
end;

procedure TIntDictionary.Resize(NewCapacity: Integer);
var
  NewBuckets: TBucketArray;
  i, j: Integer;
  index: Integer;
  newLen: Integer;
begin
  if NewCapacity > 0 then
  begin
    SetLength(NewBuckets, NewCapacity);

    for i := Low(FBuckets) to High(FBuckets) do
    begin
      for j := Low(FBuckets[i]) to High(FBuckets[i]) do
      begin
        if not CompareMem(@FEmptyItem, @FBuckets[i][j], SizeOfItem) then  // bucket is not empty
        begin
          index := FBuckets[i][j].Key mod NewCapacity;
          newLen := Length(NewBuckets[index]) + 1;
          SetLength(NewBuckets[index], newLen);
          Move(FBuckets[i][j], NewBuckets[index][newLen-1], SizeOfItem);
        end;
      end;
      SetLength(FBuckets[i], 0);
    end;

    SetLength(FBuckets, 0);
    FBuckets := NewBuckets;
    FCapacity := NewCapacity;
  end;
end;

procedure TIntDictionary.SetCapacity(const Value: Integer);
begin
  if (FCapacity <> Value) then
  begin
    if Value > 0 then
    begin
      if Count = 0 then
      begin
        FCapacity := Value;
        SetLength(FBuckets, FCapacity);
      end else begin
        Resize(Value);
      end;
    end else
    if Value = 0 then
    begin
      FCapacity := Value;
      Clear;
    end;
  end;
end;

end.
