unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    Memo2: TMemo;
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Edit2: TEdit;
    Label4: TLabel;
    Edit3: TEdit;
    Label5: TLabel;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses
  StrUtilsEx;

function StringReplace1(const source, oldPattern, newPattern : string ) : string;
var
  sb: TStringBuilder;
begin
  sb := TStringBuilder.Create;
  try
    sb.Append(source);
    sb.Replace(oldPattern, newPattern);
    Result := sb.ToString;
  finally
    FreeAndNil(sb);
  end;
end;

function StringReplace2(const source, oldPattern, newPattern : string ) : string;
const
  _32kb = 32 * 1024;
var
  si, pi, slen, plen, p : Integer;
  sb: TStringBuilder;
begin
  p := Pos(oldPattern, source);
  if p = 0 then
  begin
    Result := source;
    exit;
  end;

  Result := '';
  si := 0;
  pi := 0;
  slen := Length(source);
  plen := Length(oldPattern);

  if slen < 200 then
  begin
    result := StringReplace(source, oldPattern, newPattern, [rfReplaceAll]);
  end
  else
  if slen < _32kb then
  begin
    sb := TStringBuilder.Create;
    try
      sb.Append(source);
      sb.Replace(oldPattern, newPattern, p - 1, slen - p + 1);
      Result := sb.ToString;
    finally
      FreeAndNil(sb);
    end;
  end else
  begin
    while( si < slen ) do begin
      if source[si + 1] = oldPattern[pi + 1] then begin
        pi := pi + 1;
        if pi = plen then begin
          pi := 0;
          result := result + newPattern;
        end;
      end else begin
        if pi > 0 then begin
          result := result + Copy(source, si - pi + 1, pi + 1);
          pi := 0;
        end else begin
          result := result + source[si + 1];
        end;
      end;
      si := si + 1;
    end;
  end;
end;

function StringReplace3( source, oldPattern, newPattern : string ) : string;
var
  si, pi, slen, plen : Integer;
  sc : string;
begin
  Result := '';
  si := 0;
  pi := 0;
  slen := Length(source);
  plen := Length(oldPattern);

  while( si < slen ) do begin
    sc := Copy( source, si + 1, 1);
    if sc = Copy(oldPattern, pi + 1, 1 ) then begin
      pi := pi + 1;
      if pi = plen then begin
        pi := 0;
        result := result + newPattern;
      end;
    end else begin
      if pi > 0 then begin
        result := result + Copy(source, si - pi + 1, pi + 1);
        pi := 0;
      end else begin
        result := result + sc;
      end;
    end;
    si := si + 1;
  end;
end;

// this seems to be fast when S is small, but notice that it does not have the
// rfIgnoreCase option, so it won't work like std StringReplace()
function StringReplace4(const S, Srch, Replace: string): string;
var
  I: Integer;
  Source: string;
begin
  Source := S;
  Result := '';
  repeat
    I := Pos(Srch, Source);
    if I > 0 then begin
      Result := Result + Copy(Source, 1, I - 1) + Replace;
      Source := Copy(Source, I + Length(Srch), MaxInt);
    end
    else Result := Result + Source;
  until I <= 0;
end;


procedure TForm1.Button1Click(Sender: TObject);
var
  OldPattern,
  newPattern: string;
  Count: Integer;
  i: Integer;
  s, res1, res2, res3: string;
  winner: string;
  h1, h2: Integer;
  time, minTime: Integer;

  procedure Log(FuncName: string);
  begin
    time := h2 - h1;
    if time <= minTime then begin
      minTime := time;
      winner := FuncName;
    end;
    Memo1.Lines.Add(FuncName + ': ' + FormatFloat('0.0000 secs', (time) / 1000));
  end;

begin
  s := Memo2.Lines.Text;
  OldPattern := Edit1.Text;
  NewPattern := Edit2.Text;
  Count := StrToInt(Edit3.Text);
  minTime := MaxInt;

  h1 := GetTickCount;
  for i := 1 to count do
    res2 := StringReplace(s, OldPattern, newPattern, [rfReplaceAll]);
  h2 := GetTickCount;
  Log('Standard StringReplace()');

  h1 := GetTickCount;
  for i := 1 to count do
    res1 := StringReplace1(s, OldPattern, newPattern);
  h2 := GetTickCount;
  Log('StringReplace1');

  h1 := GetTickCount;
  for i := 1 to count do
    res3 := StringReplace2(s, OldPattern, newPattern);
  h2 := GetTickCount;
  Log('StringReplace2');

  h1 := GetTickCount;
  for i := 1 to count do
    res3 := StringReplace3(s, OldPattern, newPattern);
  h2 := GetTickCount;
  Log('StringReplace3');

  h1 := GetTickCount;
  for i := 1 to count do
    res3 := StringReplace4(s, OldPattern, newPattern);
  h2 := GetTickCount;
  Log('StringReplace4');

  h1 := GetTickCount;
  for i := 1 to count do
    res3 := FastStringReplace(s, OldPattern, newPattern, [rfReplaceAll]);
  h2 := GetTickCount;
  Log('FastStringReplace');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('The winner is: ' + winner);
  Memo1.Lines.Add('------------------------------------------------------------');
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Memo2.Lines.Text := 'alksj sldkj slkfj woeiurlkk nxcvlckjiuwr lkjwel � v ,ssdnfklwejrowu werfwesd alksj sldkj slkfj woeiurlkk nxcvlckjiuwr lkjwel v ,ssdnfklwejrowu werfwesd ,ssdnfklwejrowu werfwesd alksj sldkj slkfj woeiurlkk nxcvlckjiuwr lkjwel v ,ssdnfklwejrowu werfwesd';
  Edit1.Text := 's';
  Edit2.Text := 'abcd';
  ShowMessage('Long string, Length(OldPattern) = 1, Length(OldPattern) < Length(NewPattern), multiple occurrences of the search pattern');
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  Memo2.Lines.Text := 'alksj sldkj slkfj woeiurlkk nxcvlckjiuwr lkjwel � v ,ssdnfklwejrowu werfwesd alksj sldkj slkfj woeiurlkk nxcvlckjiuwr lkjwel v ,ssdnfklwejrowu werfwesd ,ssdnfklwejrowu werfwesd alksj sldkj slkfj woeiurlkk nxcvlckjiuwr lkjwel v ,ssdnfklwejrowu werfwesd';
  Edit1.Text := 's';
  Edit2.Text := 'k';
  ShowMessage('Long string, Length(OldPattern) = 1, Length(OldPattern) = Length(NewPattern), multiple occurrences of the search pattern');
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  Memo2.Lines.Text := 'abcdefg';
  Edit1.Text := 'c';
  Edit2.Text := 'm';
  ShowMessage('Small string, Length(OldPattern) = 1, Length(OldPattern) = Length(NewPattern), single occurrence of the search pattern');
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  Memo2.Lines.Text := 'abcdefg';
  Edit1.Text := 'c';
  Edit2.Text := 'xyz';
  ShowMessage('Small string, Length(OldPattern) = 1, Length(OldPattern) < Length(NewPattern), single occurrence of the search pattern');
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  Memo2.Lines.Text := 'alksj sldkj slkfj woeiurlkk nxcvlckjiuwr lkjwel � v ,ssdnfklwejrowu werfwesd alksj sldkj slkfj woeiurlkk nxcvlckjiuwr lkjwel v ,ssdnfklwejrowu werfwesd ,ssdnfklwejrowu werfwesd alksj sldkj slkfj woeiurlkk nxcvlckjiuwr lkjwel v ,ssdnfklwejrowu werfwesd';
  Edit1.Text := 'this won''t be found';
  Edit2.Text := 'abcd';
  ShowMessage('Long string, Length(OldPattern) = 19, Length(OldPattern) > Length(NewPattern), ZERO occurrences of the search pattern');
end;

procedure TForm1.Button7Click(Sender: TObject);
var
  s: string;
begin
  s := 'alksj sldkj slkfj woeiurlkk nxcvlckjiuwr lkjwel � v ,ssdnfklwejrowu werfwesd alksj sldkj slkfj woeiurlkk nxcvlckjiuwr lkjwel v ,ssdnfklwejrowu werfwesd ,ssdnfklwejrowu werfwesd alksj sldkj slkfj woeiurlkk nxcvlckjiuwr lkjwel v ,ssdnfklwejrowu werfwesd';
  Memo2.Lines.Text := s + s + s + s + s + s + s + s + s + s;
  Edit1.Text := 's';
  Edit2.Text := 'l';
  ShowMessage('Really long string, Length(OldPattern) = 1, Length(OldPattern) = Length(NewPattern), huge number of occurrences of the search pattern');
end;

end.
